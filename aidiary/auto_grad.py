#%%[markdown]
# # [PyTorch 自動微分](http://aidiary.hatenablog.com/entry/20180129/1517233796)

#%%
# ライブラリのインポート
import numpy as np
import torch
import torch.nn as nn

#%%
# テンソルを作成
# requires_grad=Falseだと微分の対象にならず勾配はNoneが返る
x = torch.tensor(1.0, requires_grad=True)
w = torch.tensor(2.0, requires_grad=True)
b = torch.tensor(3.0, requires_grad=True)

# 計算グラフを構築
# y = 2 * x + 3
y = w * x + b

# 勾配を計算
y.backward()

# 勾配を表示
print(x.grad)  # dy/dx = w = 2
print(w.grad)  # dy/dw = x = 1
print(b.grad)  # dy/db = 1

#%%[markdown]
# - requires_grad=Falseだと微分の対象にならず勾配はNoneが返る
# - requires_grad=False はFine-tuningで層のパラメータを固定したいときに便利
# - 計算グラフを構築してbackward()を実行するとグラフを構築する各変数のgradに勾配が入る


#%%[markdown]
# $y = x^2$
#
# $\frac{dy}{dx} = 2x$

#%%
x = torch.tensor(2.0, requires_grad=True)
y = x ** 2
y.backward()
print(x.grad)

#%%[markdown]
# yは変数xの式で成り立っていて、yの``backward()``を呼び出すとそれぞれの変数のgradプロパティに勾配が入る。

#%%[markdown]
# $y=\mathrm{e}^{x}$
#
# $\frac{dy}{dx} = \mathrm{e}^{x}$

#%%
x = torch.tensor(2.0, requires_grad=True)
y = torch.exp(x)
y.backward()
print(x.grad)


#%%[markdown]
# - 計算グラフを構築するときは numpy の関数 numpy.exp() を使ってはダメ
# - テンソル計算を行う専用の関数を使う torch.exp()
# - これらの関数は微分可能なので計算グラフ上で誤差逆伝搬が可能

#%%[markdown]
# $y=\sin{x}$
#
# $\frac{dy}{dx} = \cos{x}$

#%%
x = torch.tensor(np.pi, requires_grad=True)
y = torch.sin(x)
y.backward()
print(x.grad)

#%%[markdown]
# $y = (x-4)(x^2+6)$
#
# $\frac{dy}{dx} = 3^2-8x+6$

#%%
x = torch.tensor(0.0, requires_grad=True)
y = (x - 4)*(x ** 2 + 6)
y.backward()
print(x.grad)

#%%[markdown]
# $y = (\sqrt{x}+1)^3$
#
# $\frac{dy}{dx} = \frac{3(\sqrt{x}+1)^2}{2\sqrt{x}}$

#%%
x = torch.tensor(2.0, requires_grad=True)
y = (torch.sqrt(x) + 1) ** 3
y.backward()
print(x.grad)

#%%[markdown]
# $z = (x+2y)^2$
# 
# $\frac{\partial z}{\partial x} = 2(x+2y)$
# 
# $\frac{\partial z}{\partial y} = 4(x+2y)$


#%%
x = torch.tensor(1.0, requires_grad=True)
y = torch.tensor(2.0, requires_grad=True)
z = (x + 2 * y) ** 2
z.backward()
print(x.grad)  # dz/dx
print(y.grad)  # dz/dy

#%%[markdown]
# ### lossを微分する
#
# ニューラルネットの場合は、**lossをパラメータ（重みやバイアス）で偏微分**した値を使って勾配降下法でパラメータを更新するのが一般的。

#%%
# バッチサンプル数=5、入力特徴量の次元数=3
x = torch.randn(5, 3)
# バッチサンプル数=5、出力特徴量の次元数=2
y = torch.randn(5, 2)

# Linear層を作成
# 3ユニット => 2ユニット
linear = nn.Linear(3, 2)

# Linear層のパラメータ
print('w:', linear.weight)
print('b:', linear.bias)

# lossとoptimizer
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(linear.parameters(), lr=0.01)

# forward
pred = linear(x)

# loss = L
loss = criterion(pred, y)
print('loss:', loss)

# backpropagation
loss.backward()

# 勾配を表示
print('dL/dw:', linear.weight.grad)
print('dL/db:', linear.bias.grad)

# 勾配を用いてパラメータを更新
print('*** by hand')
print(linear.weight.sub(0.01 * linear.weight.grad))
print(linear.bias.sub(0.01 * linear.bias.grad))

# 勾配降下法
optimizer.step()

# 1ステップ更新後のパラメータを表示
# 上の式と結果が一致することがわかる
print('*** by optimizer.step()')
print(linear.weight)
print(linear.bias)
