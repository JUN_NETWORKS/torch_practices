#%%[markdown]
# ## [Pytorch Linear Regression](http://aidiary.hatenablog.com/entry/20180131/1517387821)

#%%[markdown]
# まずは基本ということで線形回帰（Linear Regression）から。人工データとBoston house price datasetを試してみた。まだ簡単なのでCPUモードのみ。GPU対応はまた今度。

#%%[markdown]
# #### 人工データセット

#%%
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt

# hyper parameters
input_size = 1
output_size = 1
num_epochs = 60
learning_rate = 0.001

#%%[markdown]
# #### データセット作成

#%%
# toy dataset
# 15 samples, 1 features
x_train = np.array([3.3, 4.4, 5.5, 6.71, 6.93, 4.168, 9.779, 6.182, 7.59, 2.167,
                    7.042, 10.791, 5.313, 7.997, 3.1], dtype=np.float32)

y_train = np.array([1.7, 2.76, 2.09, 3.19, 1.694, 1.573, 3.366, 2.596, 2.53, 1.221,
                    2.827, 3.465, 1.65, 2.904, 1.3], dtype=np.float32)

x_train = x_train.reshape(15, 1)
y_train = y_train.reshape(15, 1)

#%%[markdown]
# ``nn.Linear``` への入力は ``(N,∗,in_features)`` であるため ``reshape`` が必要。``*`` には任意の次元を追加できるが今回は1次元データなのでない。

#%%[markdown]
# #### モデルを構築

#%%
# linear regression model
class LinearRegression(nn.Module):

    def __init__(self, input_size, output_size):
        super(LinearRegression, self).__init__()
        self.linear = nn.Linear(input_size, output_size, bias=True)

    def forward(self, x):
        out = self.linear(x)
        return out

model = LinearRegression(input_size, output_size)

#%%[markdown]
# PyTorchのモデルはChainerと似ている。
# 
# - nn.Module を継承したクラスを作成
# - __init__() に層オブジェクトを定義
# - forward() に順方向の処理

#%%[markdown]
# #### Loss と Optimizer

#%%
# loss and optimizer
criterion = nn.MSELoss()  # "criterion" 意味: ｢基準｣
optimizer = optim.SGD(model.parameters(), lr=learning_rate)

#%%[markdown]
# - 線形回帰なので平均二乗誤差（mean squared error）
# - OptimizerはもっともシンプルなStochastic Gradient Descentを指定

#%%[markdown]
# #### 訓練ループ
#%%
# train the model
for epoch in range(num_epochs):
    inputs = torch.from_numpy(x_train)
    targets = torch.from_numpy(y_train)

    optimizer.zero_grad()
    outputs = model(inputs)
    loss = criterion(outputs, targets)
    loss.backward()
    optimizer.step()

    if (epoch + 1) % 10 == 0:
        print('Epoch [%d/%d], Loss: %.4f' % (epoch + 1, num_epochs, loss.item()))

# save the model
torch.save(model.state_dict(), "linear_regression_model.pkl")

#%%[markdown]
# - 各エポックでは zero_grad()で勾配をクリアすること！
# - パラメータはoptimizer.step() で更新される
# - 10エポックごとに訓練lossを表示する
# - 最後にモデルを保存

#%%[markdown]
# 訓練データと予測した直線を描画
#%%
# plot the graph
predicted = model(torch.from_numpy(x_train)).detach().numpy()  # ndarray => tensor => predict => ndarray
plt.plot(x_train, y_train, 'ro', label='Original data')
plt.plot(x_train, predicted, label='Fitted line')
plt.legend()
plt.show()

#%%[markdown]
# 勾配（grad）を持っているTensorはそのままnumpy arrayに変換できない。detach() が必要
# 
# > RuntimeError: Can't call numpy() on Variable that requires grad. Use var.detach().numpy() instead.
#
# [Differences between .data and .detach](https://github.com/pytorch/pytorch/issues/6990#issuecomment-384680164)
#%%[markdown]
# ### Boston house price dataset

#%%[markdown]
# 次は家の価格のデータセットもやってみよう。13個の特徴量をもとに家の値段を予測する。入力層が13ユニットで出力層が1ユニットの線形回帰のネットワークを書いた。

#%%
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# hyper parameters
# hyper parameters
input_size = 13
output_size = 1
num_epochs = 5000
learning_rate = 0.01

boston = load_boston()
X = boston.data
y = boston.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 5)

# データの標準化
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

y_train = np.expand_dims(y_train, axis=1)
y_test = np.expand_dims(y_test, axis=1)

# linear regression model
class LinearRegression(nn.Module):

    def __init__(self, input_size, output_size):
        super(LinearRegression, self).__init__()
        self.linear = nn.Linear(input_size, output_size)

    def forward(self, x):
        out = self.linear(x)
        return out

model = LinearRegression(input_size, output_size)

# loss and optimizer
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

def train(X_train, y_train):
    inputs = torch.from_numpy(X_train).float()
    targets = torch.from_numpy(y_train).float()

    optimizer.zero_grad()
    outputs = model(inputs)

    loss = criterion(outputs, targets)
    loss.backward()
    optimizer.step()

    return loss.item()

def valid(X_test, y_test):
    inputs = torch.from_numpy(X_test).float()
    targets = torch.from_numpy(y_test).float()
    
    outputs = model(inputs)
    val_loss = criterion(outputs, targets)
    
    return val_loss.item()
        
# train the model
loss_list = []
val_loss_list = []
for epoch in range(num_epochs):
    # data shuffle
    perm = np.arange(X_train.shape[0])
    np.random.shuffle(perm)
    X_train = X_train[perm]
    y_train = y_train[perm]

    loss = train(X_train, y_train)
    val_loss = valid(X_test, y_test)

    if epoch % 200 == 0:
        print('epoch %d, loss: %.4f val_loss: %.4f' % (epoch, loss, val_loss))

    loss_list.append(loss)
    val_loss_list.append(val_loss)

# plot learning curve
plt.plot(range(num_epochs), loss_list, 'r-', label='train_loss')
plt.plot(range(num_epochs), val_loss_list, 'b-', label='val_loss')
plt.legend()
