#%%[markdown]
# VGG16のpretrainモデルをfine-tuningしてDogとCatの分類をする

#%%
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torchvision
from torchvision import datasets

#%%[markdown]
# ### データ準備
# Kaggleの[イヌとネコのコンペサイト](https://www.kaggle.com/c/dogs-vs-cats-redux-kernels-edition)から画像 ``train.zip`` と ``test.zip`` をダウンロードする。kaggle-cliというコマンドラインツールを使うと、Kaggleにログインした上でコマンドでデータのダウンロードができるのでとても便利！
#
# 各種ディレクトリのパスを定義する。

#%%
import os
current_dir = os.getcwd()
data_dir = os.path.join(current_dir, "data", "dogscats")
train_dir = os.path.join(data_dir, "train")
valid_dir = os.path.join(data_dir, "valid")
test_dit = os.path.join(data_dir, "test")

#%%[markdown]
# 訓練画像は25000枚、テスト画像は15000枚。訓練データからランダムに選んだ2000枚をバリデーションデータとする。

#%%
os.chdir(train_dir)  # change current dir
import os
from glob import glob
import numpy as np
g = glob("*.jpg")
shuf = np.random.permutation(g)
for i in range(2000):
    os.rename(shuf[i], os.path.join(valid_dir, shuf[i]))

#%%[markdown]
# PyTorchで読み込みやすいようにクラスごとにサブディレクトリを作成する。Kaggleのテストデータは正解ラベルがついていないため ``unknown`` というサブディレクトリにいれる.

#%%
