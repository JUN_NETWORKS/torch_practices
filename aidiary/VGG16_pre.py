#%%[markdown]
# 学習済みのVGG16を使ってImageNetの1000クラスの画像分類を試す

#%%
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms

import json
import numpy as np
from PIL import Image

#%%[markdown]
# ### モデルのロード
#
# VGG16は ``vgg16`` クラスとして実装されている。``pretrained=True`` にするとImageNetで学習済みの重みがロードされる

#%%
vgg16 = models.vgg16(pretrained=True)

#%%[markdown]
# ``print(vgg16)`` するとモデル構造が表示される。``(features)``と``(classifier)``の2つのSequentialモデルから成り立っていることがわかる。VGG16は ``Conv2d => ReLU => MaxPool2d`` の繰り返しからなる単純な構造。

#%%
vgg16

#%%[markdown]
# **推論するときは eval() で評価モードに切り替えること！**
#
# > Some models use modules which have different training and evaluation behavior, such as batch normalization. To switch between these modes, use model.train() or model.eval() as appropriate. See train() or eval() for details. http://pytorch.org/docs/master/torchvision/models.html

#%%
vgg16.eval()

#%%[markdown]
# ### 前処理
#
# ImageNetで学習した重みを使うときはImageNetの学習時と同じデータ標準化を入力画像に施す必要がある。
# > All pre-trained models expect input images normalized in the same way, i.e. mini-batches of 3-channel RGB images of shape (3 x H x W), where H and W are expected to be at least 224. The images have to be loaded in to a range of [0, 1] and then normalized using mean = [0.485, 0.456, 0.406] and std = [0.229, 0.224, 0.225]. You can use the following transform to normalize: http://pytorch.org/docs/master/torchvision/models.html

#%%
normalize = transforms.Normalize(
    mean=[0.485, 0.485, 0.485],
    std=[0.229, 0.229, 0.229]
)

preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    normalize
])

#%%[markdown]
# 入力画像に対して以下の変換を施している。
#
# - 256 x 256にリサイズ
# - 画像の中心部分の 224 x 224 のみ取り出す
# - テンソルに変換
# - ImageNetの訓練データのmeanを引いてstdで割る（標準化）
#
# 試しに画像を入れてみよう。PyTorchでは基本的に画像のロードはPILを使う。先ほど作成した preprocessに通してみよう。


#%%
img = Image.open("aidiary/data/test.jpg")
img_tensor = preprocess(img)
print(img_tensor.shape)


#%%[markdown]
# 画像が3Dテンソルに変換される。

# - Keras (TensorFlow）と違ってチャンネルが前にくる
# - バッチサイズがついた4Dテンソルではない
# ことに注意。画像として表示したい場合は ToTensor() する前までの変換関数を用意すればよい。これならPILのままなので普通にJupyter Notebook上で画像描画できる。



#%%
preprocess2 = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224)
])
trans_img = preprocess2(img)
print(type(trans_img))  # <class 'PIL.Image.Image'>
trans_img

#%%[markdown]
# ### 画像分類
# 画像をモデルに入力するときは3Dテンソルではなく、**バッチサイズの次元を先頭に追加した4Dテンソルにする必要がある。**次元を増やすのは ``unsqueeze_()`` でできる。アンダーバーがついた関数はIn-placeで属性を置き換える。



#%%
img_tensor.unsqueeze_(0)
print(img_tensor.size())

#%%[markdown]
# モデルへ入れるときは4DテンソルをVariableに変換する必要がある

#%%
out = vgg16(Variable(img_tensor))
print(out.size())

#%%[markdown]
# outはsoftmaxを取る前の値なので確率になっていない（足して1.0にならない）。だが、分類するときは確率にする必要がなく、出力が最大値のクラスに分類すればよい。


#%%
np.argmax(out.data.numpy())

#%%[markdown]
# 出力が大きい順にtop Kを求めたいときは ``topk()`` という関数がある。

#%%
out.topk(5)

#%%[markdown]
# 下のように出力とそのインデックスが返ってくる。

#%%[markdown]
# ImageNetの1000クラスの332番目のインデックスのクラスに分類されたけどこれはなんだろう？

#%%[markdown]
# ### ImageNetの1000クラスラベル
# PyTorchにはImageNetの1000クラスのラベルを取得する機能はついていないようだ。ImageNetの1000クラスのラベル情報は[ここ](https://s3.amazonaws.com/deep-learning-models/image-models/imagenet_class_index.json)からJSON形式でダウンロードできるので落とす。

#%%
!wget https://s3.amazonaws.com/deep-learning-models/image-models/imagenet_class_index.json

#%%
class_index = json.load(open("aidiary/data/imagenet_class_index.json", "r"))
print(class_index)

#%%
labels = {int(key):value for (key, value) in class_index.items()}
print(labels[0])   # ['n01440764', 'tench']
print(labels[1])    # ['n01443537', 'goldfish']

#%%[markdown]
# 予測したインデックスのクラスは

#%%
print(labels[np.argmax(out.data.numpy())])

#%%[markdown]
# ### テスト
# 関数化していくつかの画像で評価

#%%
def predict(image_file):
    img = Image.open(image_file)
    img_tensor = preprocess(img)
    img_tensor.unsqueeze_(0)

    out = vgg16(img_tensor)

    # 出力を確率にする（分類するだけなら不要）
    out = nn.functional.softmax(out, dim=1)
    out = out.data.numpy()

    maxid = np.argmax(out)
    maxprob = np.max(out)
    label = labels[maxid]
    return img, label, maxprob

#%%
img, label, prob = predict("aidiary/data/test.jpg")
print(label, prob)
img

#%%
