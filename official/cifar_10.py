#%%[markdown]
# We will do the following steps in order:

# 1.Load and normalizing the CIFAR10 training and test datasets using torchvision
# 2.Define a Convolution Neural Network
# 3.Define a loss function
# 4.Train the network on the training data
# 5.Test the network on the test data

#%%[markdown]
# ## Loading and normalizing CIFAR10

#%%
import torch
import torchvision
import torchvision.transforms as transforms

#%%
transform = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ]
)

train_set = torchvision.datasets.CIFAR10(root="./data", train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(train_set, batch_size=4,
                                        shuffle=True, num_workers=2)

test_set = torchvision.datasets.CIFAR10(root="./data", train=False,
                                        download=True, transform=transform)
testloader = torch.utils.data.DataLoader(test_set, batch_size=4,
                                        shuffle=True, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

#%%[markdown]
# データを見てみる

#%%
import matplotlib.pyplot as plt
%matplotlib inline
import numpy as np

def imshow(img):
    img = img / 2 + 0.5  # 正則化したものを戻す
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

# get random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(4)))

#%%[markdown]
# ## Define a Convolution Neural Network


#%%
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, (5,5))  # nn.Conv2D(input_channnels, output_channnels, kernel_size)
        self.pool = nn.MaxPool2d((2,2), 2)
        self.conv2 = nn.Conv2d(6, 16, 5)  # kernel size が (n,n) のときは略して n だけでいける
        self.fc1 = nn.Linear(16 * 5 * 5, 120)  # nn.Linear()について: https://pytorch.org/docs/stable/nn.html#linear
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()

#%%[markdown]
# ## Define a Loss function and optimizer

#%%
import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)  # Momentum SGD

#%%[markdown]
# ## Train the network

#%%
for epoch in range(2):  # epochs: 1つの訓練データセットを何回繰り返して学習させるのか

    running_loss = 0.0
    for i, data in enumerate(trainloader):
        # get the inputs
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize  (誤差逆伝播法)
        outputs = net(inputs)  # 順伝播で予測を出力
        loss = criterion(outputs, labels)  # 予測と正解を元に損失(誤差)計算
        loss.backward()  # 逆伝播
        optimizer.step()  # パラメータの更新

        # print statistics (進捗表示)
        running_loss += loss.item()  # If you have a one element tensor, use .item() to get the value as a Python number
        if i % 2000 == 1999:  # 2000 mini-batches ごとに表示
            print("[{0}, {1}] loss: {2:.3f}".format(epoch + 1, i+1, running_loss / 2000))
            running_loss = 0.0

print("Finished Training")

#%%[markdown]
# ## Test the network on the test data

#%%
dataiter = iter(testloader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(4)))

#%%[markdown]
# どのように出力されているか見てみる

#%%
outputs = net(images)

#%%
outputs

#%%
outputs.shape

#%%[markdown]
# 出力は画像ごとに10クラスで、もっとも高いインデックスのクラスが予想クラス

#%%
_, predictions = torch.max(outputs, 1)

print("Predictions: ", "  ".join(classes[predictions[i]] for i in range(4)))

#%%[markdown]
# このネットワークのテストデータセットでの精度を見てみる

#%%
correct = 0
total = 0
with torch.no_grad():  # 勾配計算はしない  (必要ないから)
    for data in testloader:
        images, labels = data
        outputs = net(images)
        _, predictions = torch.max(outputs, 1)
        total += labels.size(0)  # サイズ(shape)の0次元目を加算
        correct += (predictions == labels).sum().item()

print("Accuracy of the network on the 10000 test images: {:.6f} %%".format(100 * (correct / total)))

#%%[markdown]
# NNなしのランダムだと精度は10%なので学習はできている。
#
# どのクラスに対する予測の精度が良い、または悪いか

#%%
class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
with torch.no_grad():
    for data in testloader:
        images, labels = data
        outputs = net(images)
        _, predictions = torch.max(outputs, 1)
        c = (predictions==labels).squeeze()  # squeeze(): サイズ1の次元を削除したTensorを返す
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1

for i in range(10):
    print("Accuracy of {0} : {1:.3f} %%".format(classes[i], 100*(class_correct[i]/class_total[i])))

#%%[markdown]
# ### TRAINING ON GPU

#%%
device = torch.device("cuda" if torch.cuda.is_available() else "cpu: 0")

print(device)
#%%[markdown]
# ここからはGPUを使う場合の話

#%%
# # パラメータとバッファをCUDA Tensorに置き換える
# net.to(device)
# # すべてのステップの入力とターゲットをGPUに送信する必要がある
# inputs, labels = inputs.to(device), labels.to(device)