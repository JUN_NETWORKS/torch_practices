#%%
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms

#%%[markdown]
# ## 8
#
# ### 8a

#%%
device = ("cuda" if torch.cuda.is_available() else "cpu")

#%%
# hyper parameters
input_size = 784
hidden_size = 100
num_classes = 5
num_epochs = 50
batch_size = 512
learning_rate = 0.001

#%%
class DNN(nn.Module):

    def __init__(self, input_size, num_classes, hidden_size, ELU_alpha=1.0):
        super(DNN, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        hidden_layers = [nn.Linear(hidden_size, hidden_size) if i%2==0 else nn.ELU(alpha=ELU_alpha, inplace=True) for i in range(10)]
        # self.fc2 = nn.Sequential(
        #     nn.Linear(hidden_size, hidden_size),
        #     nn.ELU(alpha=ELU_alpha, inplace=True),
        #     nn.Linear(hidden_size, hidden_size),
        #     nn.ELU(alpha=ELU_alpha, inplace=True),
        #     nn.Linear(hidden_size, hidden_size),
        #     nn.ELU(alpha=ELU_alpha, inplace=True),
        #     nn.Linear(hidden_size, hidden_size),
        #     nn.ELU(alpha=ELU_alpha, inplace=True),
        #     nn.Linear(hidden_size, hidden_size),
        #     nn.ELU(alpha=ELU_alpha, inplace=True),
        # )
        self.fc2 = nn.Sequential(*hidden_layers)
        self.fc3 = nn.Linear(hidden_size, num_classes)

        # 重みをHeで初期化
        self.init_weights()

    def forward(self, x):
        x = self.fc1(x)
        # print(x.size())
        x = self.fc2(x)
        # print(x.size())
        x = self.fc3(x)
        # print(x.size())
        return x

    # 全結合レイヤーの重みをHeで初期化
    def init_weights(self):
        for m in self.modules():
            if type(m) == nn.Linear:
                nn.init.xavier_uniform_(m.weight)
                m.bias.data.fill_(0.0)  # ついでにバイアスも0で初期化(これは別にいらないかな?)

#%%[markdown]
# 最後の```Linear```のあとに```softmax```を適応していないのは、損失関数を計算する torch.nn.CrossEntropyLoss の中に softmax の計算が含まれているため。<br>
# [Why does CrossEntropyLoss include the softmax function? - PyTorch Forums](https://discuss.pytorch.org/t/why-does-crossentropyloss-include-the-softmax-function/4420)

#%%
model = DNN(input_size, num_classes, hidden_size).to(device)

#%%[markdown]
# ### 8b

#%%
# 学習データの用意
from sklearn.datasets import fetch_mldata
from sklearn.model_selection import train_test_split

mnist = fetch_mldata("MNIST original")
X = mnist.data.astype(np.float32)
y = mnist.target.astype(np.int64)

# 0-4のインスタンスだけ取り出す
X = X[y<5]
y = y[y<5]

# 各値0-1に正規化
X = X/255

# convert numpy to Tensor
tensor_X = torch.stack([torch.from_numpy(np.array(i)) for i in X]).float()  # .float(): PyTorchで求められる型に変換する
tensor_y = torch.stack([torch.from_numpy(np.array(i)) for i in y]).long()

train_size = int(len(X)*0.8)

X_train = tensor_X[:train_size]
y_train = tensor_y[:train_size]
X_test = tensor_X[train_size:]
y_test = tensor_y[train_size:]

train_dataset = torch.utils.data.TensorDataset(X_train, y_train)
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size= 100, shuffle=True)

test_dataset = torch.utils.data.TensorDataset(X_test, y_test)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size= 100, shuffle=True)

#%%
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

#%%
# 訓練
def train(train_loader):
    model.train()
    running_loss = 0.0
    for (images, labels) in train_loader:
        images = images.to(device)
        labels = labels.to(device)
        images = images.view(-1, 28*28)

        optimizer.zero_grad()
        outputs = model(images)

        loss = criterion(outputs, labels)
        running_loss += loss

        loss.backward()
        optimizer.step()

    train_loss = running_loss / len(train_loader)

    return train_loss

def valid(test_loader):
    model.eval()
    running_loss = 0
    correct = 0
    total = 0
    
    with torch.no_grad():
        for (images, labels) in test_loader:
            images = images.to(device)
            labels = labels.to(device)
            images = images.view(-1, 28 * 28)

            outputs = model(images)

            loss = criterion(outputs, labels)
            running_loss += loss.item()

            _, predicted = torch.max(outputs, 1)
            correct += (predicted == labels).sum().item()
            total += labels.size(0)

    val_loss = running_loss / len(test_loader)
    val_acc = float(correct) / total
    
    return val_loss, val_acc

# logging
loss_list = []
val_loss_list = []
val_acc_list = []

# Early Stopping
best_loss = np.Infinity
stop_count = 0

for epoch in range(num_epochs):
    train_loss = train(train_loader)
    val_loss, val_acc = valid(test_loader)

    loss_list.append(train_loss)
    val_loss_list.append(val_loss)
    val_acc_list.append(val_acc)

    print("{} epoch  train_loss: {:.4f}  val_loss: {:.4f}  val_acc: {:.4f}".format(epoch+1, train_loss, val_loss, val_acc))

    if best_loss > val_loss:
        best_loss = val_loss
        # torch.save(model.state_dict(), "mlbook_11_8_b")  # 重み保存
    else:
        stop_count += 1
        if stop_count == 15:
            print("Early Stopping!!")
            print("Best loss: {:.4f}".format(best_loss))
            break

#%%[markdown]
# 99%の精度が出た

#%%
# 学習曲線
# グラフ化
import matplotlib.pyplot as plt
%matplotlib inline

# plot learning curve
plt.figure()
plt.plot(range(epoch+1), loss_list, 'r-', label='train_loss')
plt.plot(range(epoch+1), val_loss_list, 'b-', label='val_loss')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.grid()

plt.figure()
plt.plot(range(epoch+1), val_acc_list, 'g-', label='val_acc')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('acc')
plt.grid()

#%%[markdown]
# ### 8c

#%%[markdown]
# ### Skorchを使ってGridSearchCV

#%%
# データ準備
mnist = fetch_mldata("MNIST original")
X = mnist.data.astype(np.float32)
y = mnist.target.astype(np.int64)

# 0-4のインスタンスだけ取り出す
X = X[y<5]
y = y[y<5]

# 各値0-1に正規化
X = X/255

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

from skorch import NeuralNetClassifier
clf = NeuralNetClassifier(
    DNN(input_size, num_classes, hidden_size),
    optimizer = optim.Adam,  # default=torch.optim.SGD
    criterion=torch.nn.CrossEntropyLoss,  # default=torch.nn.NLLLoss
    max_epochs = num_epochs,
    lr=learning_rate,  # default=0.01
    iterator_train__batch_size=128, # default=128
    iterator_train__shuffle=True,
    device=device,  # GPUに対応
    )


from sklearn.model_selection import GridSearchCV
params = {
    "lr": [0.01, 0.1],
    "iterator_train__batch_size": [128, 256, 512],
}

grid_cv = GridSearchCV(clf, params, cv=3, scoring="accuracy")
grid_cv.fit(X, y)

#%%
grid_cv.best_params_

#%%
grid_cv.best_estimator_

#%%[markdown]
# ### 8d

#%%
# バッチ正規化を活性化関数前に加える
class DNN_batchnorm(nn.Module):

    def __init__(self, input_size, num_classes, hidden_size, ELU_alpha=1.0):
        super(DNN_batchnorm, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        hidden_layers = [nn.Linear(hidden_size, hidden_size) if i%3==0 else nn.BatchNorm1d(hidden_size,) if i%3==1 else nn.ELU(alpha=ELU_alpha, inplace=True) for i in range(15)]
        self.fc2 = nn.Sequential(*hidden_layers)
        self.fc3 = nn.Linear(hidden_size, num_classes)

        # 重みをHeで初期化
        self.init_weights()

    def forward(self, x):
        x = self.fc1(x)
        # print(x.size())
        x = self.fc2(x)
        # print(x.size())
        x = self.fc3(x)
        # print(x.size())
        return x

    # 全結合レイヤーの重みをHeで初期化
    def init_weights(self):
        for m in self.modules():
            if type(m) == nn.Linear:
                nn.init.xavier_uniform_(m.weight)
                m.bias.data.fill_(0.0)  # ついでにバイアスも0で初期化(これは別にいらないかな?)

model = DNN_batchnorm(input_size, num_classes, hidden_size).to(device)

#%%
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# logging
loss_list = []
val_loss_list = []
val_acc_list = []

# Early Stopping
best_loss = np.Infinity
stop_count = 0

for epoch in range(num_epochs):
    train_loss = train(train_loader)
    val_loss, val_acc = valid(test_loader)

    loss_list.append(train_loss)
    val_loss_list.append(val_loss)
    val_acc_list.append(val_acc)

    print("{} epoch  train_loss: {:.4f}  val_loss: {:.4f}  val_acc: {:.4f}".format(epoch+1, train_loss, val_loss, val_acc))

    if best_loss > val_loss:
        best_loss = val_loss
        torch.save(model.state_dict(), "mlbook_11_8_d")  # 重み保存
    else:
        stop_count += 1
        if stop_count == 15:
            print("Early Stopping!!")
            print("Best loss: {:.4f}".format(best_loss))
            break

#%%
# 学習曲線
# グラフ化
import matplotlib.pyplot as plt
%matplotlib inline

# plot learning curve
plt.figure()
plt.plot(range(epoch+1), loss_list, 'r-', label='train_loss')
plt.plot(range(epoch+1), val_loss_list, 'b-', label='val_loss')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.grid()

plt.figure()
plt.plot(range(epoch+1), val_acc_list, 'g-', label='val_acc')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('acc')
plt.grid()

#%%
model.load_state_dict(torch.load("mlbook_11_8_d", 
                                map_location=lambda storage,
                                loc: storage))

model.eval()

from sklearn.metrics import accuracy_score
images, labels = iter(test_loader).__next__()
images = images.to(device)
images = images.view(-1, 28 * 28)
outputs = model(images)
_, predicted = torch.max(outputs, 1)

labels = labels.cpu().numpy()
predicted = predicted.cpu().numpy()

accuracy_score(labels, predicted)


#%%[markdown]
# ### 8e

#%%
# Dropoutを追加する
class DNN_dropout(nn.Module):

    def __init__(self, input_size, num_classes, hidden_size, ELU_alpha=1.0):
        super(DNN_dropout, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        hidden_layers = [nn.Linear(hidden_size, hidden_size) if i%4==0 else nn.BatchNorm1d(hidden_size,) if i%4==1 else nn.ELU(alpha=ELU_alpha, inplace=True) if i%4==2 else nn.Dropout(p=0.5) for i in range(20)]
        self.fc2 = nn.Sequential(*hidden_layers)
        self.fc3 = nn.Linear(hidden_size, num_classes)

        # 重みをHeで初期化
        self.init_weights()

    def forward(self, x):
        x = self.fc1(x)
        # print(x.size())
        x = self.fc2(x)
        # print(x.size())
        x = self.fc3(x)
        # print(x.size())
        return x

    # 全結合レイヤーの重みをHeで初期化
    def init_weights(self):
        for m in self.modules():
            if type(m) == nn.Linear:
                nn.init.xavier_uniform_(m.weight)
                m.bias.data.fill_(0)

model = DNN_dropout(input_size, num_classes, hidden_size).to(device)
model
#%%
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# logging
loss_list = []
val_loss_list = []
val_acc_list = []

# Early Stopping
best_loss = np.Infinity
stop_count = 0

for epoch in range(num_epochs):
    train_loss = train(train_loader)
    val_loss, val_acc = valid(test_loader)

    loss_list.append(train_loss)
    val_loss_list.append(val_loss)
    val_acc_list.append(val_acc)

    print("{} epoch  train_loss: {:.4f}  val_loss: {:.4f}  val_acc: {:.4f}".format(epoch+1, train_loss, val_loss, val_acc))

    if best_loss > val_loss:
        best_loss = val_loss
        torch.save(model.state_dict(), "mlbook_11_8_e")  # 重み保存
    else:
        stop_count += 1
        if stop_count == 15:
            print("Early Stopping!!")
            print("Best loss: {:.4f}".format(best_loss))
            break

#%%
# 学習曲線
# グラフ化
import matplotlib.pyplot as plt
%matplotlib inline

# plot learning curve
plt.figure()
plt.plot(range(epoch+1), loss_list, 'r-', label='train_loss')
plt.plot(range(epoch+1), val_loss_list, 'b-', label='val_loss')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.grid()

plt.figure()
plt.plot(range(epoch+1), val_acc_list, 'g-', label='val_acc')
plt.legend()
plt.xlabel('epoch')
plt.ylabel('acc')
plt.grid()

#%%[markdown]
# おおおお！！過学習がかなり緩和された！！


#%%

model.load_state_dict(torch.load("mlbook_11_8_e", 
                                map_location=lambda storage,
                                loc: storage))

model.eval()

from sklearn.metrics import accuracy_score
images, labels = iter(test_loader).__next__()
images = images.to(device)
images = images.view(-1, 28 * 28)
outputs = model(images)
_, predicted = torch.max(outputs, 1)

labels = labels.cpu().numpy()
predicted = predicted.cpu().numpy()

accuracy_score(labels, predicted)


#%%[markdown]
# 99%!! やったぜ！！ Dropout強い!!

#%%
