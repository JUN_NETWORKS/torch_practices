#%%[markdown]
# ## GoogleLeNet で CIFAR10 

#%%
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

import numpy as np

#%%
# GPU対応
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print(device)

#%%
# hyper parameters
batch_size = 256
num_classes = 10
num_epochs = 100

#%%
# データの読み込み
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root="./data", train=True, transform=transform, download=True)
train_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=4)

testset = torchvision.datasets.CIFAR10(root="./data", train=False, transform=transform)
test_loader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=4)

#%%
