#%%
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

import numpy as np

#%%
# GPU対応
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print(device)

#%%
# hyper parameters
batch_size = 256  # 論文: 256
num_classes = 10
num_epochs = 74  # 論文: 74

#%%
# データの読み込み
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root="./data", train=True, transform=transform, download=True)
train_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=4)

testset = torchvision.datasets.CIFAR10(root="./data", train=False, transform=transform)
test_loader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=4)

#%%
dataiter = iter(train_loader)
images, labels = dataiter.next()

images.shape

#%%[markdown]
# \[batch_size, channel_size, height, width\]

#%%
# モデルの定義
class VGG19(nn.Module):

    def __init__(self):
        super(VGG19, self).__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3,3), stride=1, padding=1),
            nn.BatchNorm2d(64),  # 元論文ではLRNだが、現在は正規化するならBatchNormalisationが主流
            nn.MaxPool2d(kernel_size=(2,2), stride=2),
        )
        self.block2 = nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.Conv2d(128, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2,2), stride=2),
        )
        self.block3 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(3,3), stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2,2), stride=2),
        )
        self.block4 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2,2), stride=2),
        )
        self.block5 = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3,3), stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=(2,2), stride=2),
        )
        self.block6 = nn.Sequential(
            nn.Linear(512, 4092),  # 全結合
            nn.Linear(4092, 4092),
            nn.Linear(4092, num_classes),  # 分類クラス数の特徴数で出力(CIFAR10は10クラス)
            # 他のDeepLearningフレームワークならSoftmax関数が入るが、PyTorchではCrossEntropy内にSoftmaxの計算が含まれているので書かない
        )

    def forward(self, x):
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)
        x = self.block5(x)
        x = x.view(x.size(0), -1)
        x = self.block6(x)
        
        return x


model = VGG19().to(device)  # モデルをGPUに転送する
        
#%%[markdown]
# #### [VERY DEEP CONVOLUTIONAL NETWORKS FOR LARGE-SCALE IMAGE RECOGNITION](https://arxiv.org/pdf/1409.1556.pdf)
#
# 論文の**ARCHITECTURE**を元に層を構築。いくつかの層ごとにブロックで区切って実装してみた。
#
# PyTorchでCrossEntropy内にsoftmaxが含まれている理由: [Why does CrossEntropyLoss include the softmax function? - PyTorch Forums](https://discuss.pytorch.org/t/why-does-crossentropyloss-include-the-softmax-function/4420)
#%%
# optimizerとcriterion(評価基準)を定義
import torch.optim as optim

critetion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

#%%
model.train()  # 訓練モード

for epoch in range(num_epochs):

    running_loss = 0.0
    for i, data in enumerate(train_loader):
        # get the inputs
        inputs, labels = data
        inputs = inputs.to(device)  # TensorデータをGPUに転送する
        labels = labels.to(device)

        # 勾配を0で初期化!
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = model(inputs)  # 順伝播で推論
        loss = critetion(outputs, labels)  # loss(誤差)を計算
        loss.backward()  # 逆伝播
        optimizer.step()  # パラメータ更新

        # 訓練の進捗
        running_loss += loss.item()
        # print("runnning_loss: {}".format(running_loss/(i+1)))
        
    # 1epochごとにlossを表示
    print("[{} epochs] loss: {:.5f}".format(epoch + 1, running_loss / (i+1)))
    running_loss = 0.0

print("Finished Training")

#%%
# モデルパラメータを保存
torch.save(model.state_dict(), "weights/VGG19_cifar10.pkl")

#%%
# 試しに推論してみる
iter_loader = iter(train_loader)
data = iter_loader.next()
X, y = data
outputs = model(X)
_, predictions = torch.max(outputs, 1)  # return: 各行最大値のTensorと, 各行最大値のインデックスのTensor
predictions

#%%
# 重みパラメータの読み込み
model.load_state_dict(torch.load("weights/VGG19_cifar10.pkl"))
model.eval()  # 推論モード

# テストデータを使って精度を見る
correct = 0
total = 0
model.eval()  # 評価モードに切り替える
with torch.no_grad():
    for i, (inputs, labels) in enumerate(test_loader):
        # get the inputs
        inputs = inputs.to(device)
        labels = labels.to(device)        

        outputs = model(inputs)
        _, predictions = torch.max(outputs, 1)
        total += labels.size(0)
        correct += (predictions == labels).sum().item()

print("Accuracy of test data: {}%".format((correct/total)*100))

#%%[markdown]
# *Batch Normalization*など学習時と推論時で挙動が変わるレイヤを使う場合は**モデルのモードに要注意！**
#
# - ``model.train()`` で訓練モード
# - ``model.eval()`` で評価モード

#%%
