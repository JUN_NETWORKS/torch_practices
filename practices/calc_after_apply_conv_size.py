# Convolution 層を適応したあとのサイズを計算するスクリプト

# 入力サイズ
input_size = input("Input: data height, width, in_channels: ")
height, width, in_channels = map(int, input_size.split(","))

# Conv層のサイズ
input_size = input("Conv: out_channels, kernel_h, kernel_w, stride, padding: ")
out_channels, conv_f_h, conv_f_w, conv_s, conv_p = map(int, input_size.split(","))

out_height = int((height + 2*conv_p - conv_f_h) / conv_s) + 1
out_width = int((width + 2*conv_p - conv_f_w) / conv_s) + 1

print("feature map size: [{}, {}, {}]".format(out_height, out_width, out_channels))
